BSD 3-Clause License

Copyright (c) 2019, University of Maryland
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


RAE and RAEplan together form a refinement acting-and-planning engine with multiple stacks. Each stack is implemented as a Python thread.

We have the following domains in the domains folder.

1. domain_chargeableRobot: A chargeable robot collecting different objects
2. domain_springDoor: Robot needs to collect objects in an environment with spring doors
3. domain_exploreEnv: Robots and UAV move through an area and collects various data
4. domain_industrialPlant: Orders of compound tasks involving painting, assembly and packing of components are handled

HOW TO USE?
To test on any domain, go to the folder RAE_and_RAEplan use the following command in terminal

python3 testRAEandRAEplan.py [-h] [--v V] [--domain D] [--problem P] [--plan S] [--c C] [--b breadth] [--k sampleCount]
optional arguments:
  -h, --help  	show this help message and exit
  --v V      	verbosity of RAE and RAEplan's debugging output (0, 1 or 2)
  --domain D    domain code of the test domain (STE, CR, SD, EE, SOD, IP or SF) (see below)
  --problem P   problem id for the problem eg. 'problem1', 'problem2', etc. The problem id should correspond to a
                problem inside the folder 'problems'.
  --plan pl   	Do you want to use planning or not? ('y' or 'n')
  --c C      	Mode of the clock ('Counter' or 'Clock')
  --b breadth 	Number of methods RAEplan should look at (for each task and sub-task)
  --k sampleCount 	Number of outcomes of commands RAEplan should look at (for each task and sub-task)  during planning

domain codes are as follows:
domain_chargeableRobot: 'CR',
domain_springDoor: 'SD',
domain_exploreEnv: 'EE',
domain_industrialPlan: 'IP'


HOW TO ADD NEW PROBLEMS? 
A problem file (Please go inside the folder shared/problems to view one) specifies the initial state, the tasks arriving at different times and various parameters specific to the domain. To define a new problem, please follow the
following syntax to name the file.

problemId_domainCode.py

For example, a problem of SD domain with problemId 'problem1' should be named problem1_SD.py.
To test problem1 of Spring door, use the command:

python3 testRAEandRAEplan.py --domain SD --problem problem1

The commands can be executed in two modes: 'Clock' or 'Counter'.
By default, the mode is set to 'Counter'.
