#!/bin/sh
# This script is to run RAE by calling RAE-plan

echo "Executing tests for RAE and RAE-plan."

for domain in "EE" #"CR" "SD" "IP" "EE"
do
    if [ "$domain" = "EE" ]; then
        P=("problem21" "problem22" "problem23" "problem24" "problem25" "problem26" "problem27" "problem28" "problem29" "problem30" "problem31")
        B=("1" "2" "3") 
    fi
    for problem in ${P[@]}
    do
        for b in ${B[@]}
        do 
            for k in "1" "3" "5" "7" "10" 
            do
                setup="
import sys
sys.path.append('../../RAE_and_RAEplan/')
sys.path.append('../../shared/domains/')
sys.path.append('../../shared/problems/EE')
sys.path.append('../../shared/')
sys.setrecursionlimit(6000)
from testRAEandRAEplan import globals, testBatch
globals.Setb($b)
globals.Setk($k)"
counter=1
while [ $counter -le 10 ]
do
                echo $domain $problem $b $k $counter/10
                time_test="testBatch(domain='$domain', problem='$problem', useRAEplan=True)"

                fname="$domain/random_plan_b_${b}_k_$k.txt"

                echo "Time test of $domain $problem $sampleCount" >> $fname
                python3 -m timeit -n 1 -r 1 -s "$setup" "$time_test" >> $fname
((counter++))
done
            done
        done
    done
done